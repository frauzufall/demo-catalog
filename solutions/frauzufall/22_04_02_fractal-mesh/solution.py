from album.runner.api import setup


setup(
    group='frauzufall',
    name='22_04_02_fractal-mesh',
    version='0.1.0-SNAPSHOT',
    title='Fractal mesh',
    description='This mesh was generated using the generate-3d-fractals solution.',
    album_api_version='0.3.1',
    authors=['Deborah Schmidt'],
    covers=[{
        "description": "Screenshot of the mesh displayed with VTK.",
        "source": "screenshot.png"
    }],
    documentation=["README.md"],
    custom={'data_location': '/fast/TP_ImageDataAnalysis/generate-data-app/data/output.stl'}
)
